*** Settings ***
Documentation	Testing changing the text language from finnish to swedish

Default Tags	Firefox	Language_switch

Resource	resources.robot
Suite Setup	Open Browser To Front Page
Suite Teardown	Close Browser

*** Test Cases ***
Change Language
	Page Should Contain Link	xpath=//*[@id="top-bar"]/div/div/div[2]/ul/li/a
	Click Element	xpath=//*[@id="top-bar"]/div/div/div[2]/ul/li/a
	Click Element	xpath=//*[@id="header-menu"]/div[3]/div/div/ul/li[2]/a
	#Click Link	https://www.lupapiste.fi/sv/etusivu/
	Sleep	4s
	Location Should Be	 https://www.lupapiste.fi/sv/
