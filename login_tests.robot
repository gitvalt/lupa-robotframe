*** Settings ***
Documentation	This tests if the site denies access for wrong username or password. Should return pass. 
Resource	resources.robot

*** Test Cases ***
Valid Username And Password Should Fail
	Open Browser To Front Page
	Input Username	asd
	Input Password	asd
	Submit Credentials
	Login Successful
	[Teardown]	Close Browser

Invalid Password Or Username
	Open Browser To Front Page
	Input Username	asdasd
	Input Password	asdasd
	Submit Credentials
	Incorrect Password Or Username
	[Teardown]	Close Browser

Blank Username And Password
	Open Browser To Front Page
	Submit Credentials	
	Element Text Should Be	login-message	Täytä kaikki pakolliset kentät
	[Teardown]	Close Browser