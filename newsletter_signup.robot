*** Settings ***
Documentation	Register a email for newsletters

Resource	resources.robot


Defaults Tags	Firefox_52.0
Suite Setup	Open Browser To Front Page
Suite Teardown	Close Browser

*** Variables ***
${TEST_EMAIL}	test@asd.com

*** Test Cases ***
Signup for newsletter
	Input Text	id=mce-EMAIL	${TEST_EMAIL}
	Click Button	id=mc-embedded-subscribe
	Sleep	4s
	Select Window	title=Lupapiste_-_Ammattilaiset
