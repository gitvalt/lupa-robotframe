*** Setting ***
Documentation	A resource file with reusable file with reusable keywords and variables.
...
...		The system specific keywords created here from our own
...		domain specific language. They utilize keywords provided
...		by the imported Selenium2Library
Library		Selenium2Library

*** Variables ***
${SERVER}	https://www.lupapiste.fi
${BROWSER}	Firefox
${DELAY}	1
${VALID USER}	asd
${VALID PASSWORD}	asd
${LOGIN URL}	${SERVER}/
${WELCOME URL}	${SERVER}/
${SEARCH URL}	${SERVER}/?s=moi
${SEARCH ITEM}	Jyväskylä
*** Keywords ***
Open Browser To Front Page
	Open Browser	${LOGIN URL}	${BROWSER}
	Maximize Browser Window
	Set Selenium Speed	${DELAY}
	Login Page Should Be Open

Login Page Should Be Open
	Title Should Be	Lupapiste

Go To Login Page
	Go To	${LOGIN URL}
	Login Page Should Be Open

Input Username
	[Arguments]	${username}
	Input Text	login-username	${username}

Input Password
	[Arguments]	${password}
	Input Text	login-password	${password}

Submit Credentials
	Click Button	login-button

Incorrect Password Or Username
	Element Text Should Be	login-message	Tunnus tai salasana on väärin.

Write Query
	Input Text	city-search	${SEARCH ITEM}

Write Random Query
	Input Text	search-field	${SEARCH ITEM}

Submit Query
	Click Button	submit-city-search

Should Show Results
	Element Text Should Be	city-name	${SEARCH ITEM}

Login Successful
	Element Text Should Be	login-message	Sisään kirjautuminen onnistui!
