*** Settings ***
Documentation	This test case searches for Jyväskylä in the lupapiste state search. The results should return something with word Jyväskylä in it.
Resource	resources.robot

*** Test Cases ***
Open Mainpage
	Open Browser To Front Page
	Write Query
	Submit Query
	Should Show Results
	[Teardown]	Close Browser
