*** Settings ***
Documentation   This case searches for Jyväskylä in the general search. Case will follow the site to search page. 	

Resource	resources.robot
Suite Setup	Open Browser To Front Page
Suite Teardown	Close Browser

*** Test Cases ***
Search Test
	Page Should Contain Link	xpath=//*[@id="top-bar"]/div/div/div[3]/ul[2]/li[1]/a
	Click Element	xpath=//*[@id="top-bar"]/div/div/div[3]/ul[2]/li[1]/a
	Write Random Query
	Click Button	searchsubmit
	Sleep	4s
	Location Should Be	 https://www.lupapiste.fi/?s=Jyv%C3%A4skyl%C3%A4
